FROM debian:bullseye
RUN apt -y update
RUN apt install faketime
ENV LD_PRELOAD=/lib/faketime.so
ENV FAKETIME="-1m"
ENV DONT_FAKE_MONOTONIC=1
RUN date
